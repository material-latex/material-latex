
Uma apostila de LaTeX
=====================

O projeto consiste em um pequeno conjunto de material didático 
para instrução operacional ao LaTeX. Procura-se enfatizar os 
paradigmas reforçados pelo sistema, e as metáforas de sua 
linguagem.

#### Onde consigo o pdf do material?

Uma versao recente do do material está (temporariamente) disponível
[aqui](http://www.ime.usp.br/~tassio/apostila.pdf).
